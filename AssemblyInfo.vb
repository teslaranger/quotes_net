Imports System.Reflection
Imports System.Runtime.CompilerServices
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.


' TODO: Review the values of the assembly attributes


<Assembly: AssemblyTitle("Quotes")>
<Assembly: AssemblyDescription("Displays randomly selected quotations.")>
<Assembly: AssemblyCompany("FountainWare.Net")> 
<Assembly: AssemblyProduct("Quotes")> 
<Assembly: AssemblyCopyright("Written by William  B. Davis, Jr. - Copyright � 2006-")> 
<Assembly: AssemblyTrademark("")> 
<Assembly: AssemblyCulture("")>

' Version information for an assembly consists of the following four values:

'	Major version
'	Minor Version
'	Build Number
'	Revision

' You can specify all the values or you can default the Build and Revision Numbers
' by using the '*' as shown below:

<Assembly: AssemblyVersion("2.2.*")>


