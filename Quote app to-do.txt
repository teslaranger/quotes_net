Quotes app changes
=====================

Entry screen (supporting paste) for manually entering a quote.

Font menu
Font size menu
Font style menu

Option to choose a random font from all installed fonts
Option to choose a random font from a specified list

Choose a background color menu
Random background color

Choose a font color
Random background color

Add leading/trailing quotes or not
Illuminate first letter of saying (not counting quote)

Pause/Resume random button and/or menu item to pause random display of a new quote.
Also first/last/next/prev/play buttons and a random/shuffle button.  Play goes through them sequentially:

|<    <   ||   []   |>    >    >|   %


Preferences dialog

Option to put attribution at the top & follow with : instead of at bottom preceeded with "  - " to 
allow for definitions and other items that need a top label instead of a bottom attribution

Support using a database (SQLlite by default and built-in) instead of a text file

Support CSV files

Options in brackets at top of text file that control display and other things

Document support:  *.quote or *.quot or *.qot or *.qt or *.qtxt or *.qtxt (latter 2 or 3 for text files)
  document can contain quote text or be a pointer/settings for a quote db file (*.qdb)

Quote menu with first/last/next/previous/random on it instead of file menu

Option to use a particular "frame" background around the quote from a graphics file (jpg etc)


Additional files:

Categorized quotes
Famous Birthdays
Today in History
Devils Dictionary
Devils DP Dictionary
REAL dictionary
