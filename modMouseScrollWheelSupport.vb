Option Strict Off
Option Explicit On
Module modMouseScrollWheelSupport
	Private Declare Function CallWindowProc Lib "user32.dll"  Alias "CallWindowProcA"(ByVal lpPrevWndFunc As Integer, ByVal hWnd As Integer, ByVal Msg As Integer, ByVal Wparam As Integer, ByVal Lparam As Integer) As Integer
	
	Private Declare Function SetWindowLong Lib "user32.dll"  Alias "SetWindowLongA"(ByVal hWnd As Integer, ByVal nIndex As Integer, ByVal dwNewLong As Integer) As Integer
	
	Public Const MK_CONTROL As Integer = &H8
	Public Const MK_LBUTTON As Integer = &H1
	Public Const MK_RBUTTON As Integer = &H2
	Public Const MK_MBUTTON As Integer = &H10
	Public Const MK_SHIFT As Integer = &H4
	Private Const GWL_WNDPROC As Short = -4
	Private Const WM_MOUSEWHEEL As Integer = &H20A
	
	Dim LocalHwnd As Integer
	Dim LocalPrevWndProc As Integer
	Dim MyForm As System.Windows.Forms.Form
	
	
	Private Function WindowProc(ByVal Lwnd As Integer, ByVal Lmsg As Integer, ByVal Wparam As Integer, ByVal Lparam As Integer) As Integer
		
		Dim MouseKeys As Integer
		Dim Rotation As Integer
		Dim Xpos As Integer
		Dim Ypos As Integer
		
		If Lmsg = WM_MOUSEWHEEL Then
			MouseKeys = Wparam And 65535
			Rotation = Wparam / 65536
			Xpos = Lparam And 65535
			Ypos = Lparam / 65536
			'UPGRADE_ISSUE: Control MouseWheel could not be resolved because it was within the generic namespace Form. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="084D22AD-ECB1-400F-B4C7-418ECEC5E36E"'
            'MyForm.MouseWheel(MouseKeys, Rotation, Xpos, Ypos)
		End If
		WindowProc = CallWindowProc(LocalPrevWndProc, Lwnd, Lmsg, Wparam, Lparam)
	End Function
	
	Public Sub WheelHook(ByRef PassedForm As System.Windows.Forms.Form)
		
		On Error Resume Next
		
		MyForm = PassedForm
		LocalHwnd = PassedForm.Handle.ToInt32
		'UPGRADE_WARNING: Add a delegate for AddressOf WindowProc Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="E9E157F7-EF0C-4016-87B7-7D7FBBC6EE08"'
        'LocalPrevWndProc = SetWindowLong(LocalHwnd, GWL_WNDPROC, AddressOf WindowProc)
	End Sub
	
	
	Public Sub WheelUnHook()
		Dim WorkFlag As Integer
		
		On Error Resume Next
		WorkFlag = SetWindowLong(LocalHwnd, GWL_WNDPROC, LocalPrevWndProc)
		'UPGRADE_NOTE: Object MyForm may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
		MyForm = Nothing
	End Sub
End Module