'TODO: Add a font menu
'TODO: Add a background color menu or randomly rotate colors
'TODO: Add a text color menu or randomly rotate colors
'TODO: Find code to select back/fore colors that look good together/contrast well. 
'TODO: Select a particular photo or randomly choose a photo from the Pictures folder or a selected folder.
'TODO: Find code to generate shadowed text (white text, black background.)


Option Strict Off
Option Explicit On
Option Compare Text ' important for find function!
Friend Class FrmQuote
    Inherits System.Windows.Forms.Form

    Public Const cintUnknownDelimited As Short = 0
    Public Const cintPercentDelimited As Short = 1
    Public Const cintQuoteMarkDelimited As Short = 2
    Public Const cintBlankLineDelimited As Short = 3


    Public fsQuotes As String
    Public gIntFileType As Short

    Public gstrLastFindString As String ' string last used to search
    Public glngLastFindPos As Integer ' byte loc in file after last search

    Public glngLastQuote As Integer ' last quote displayed

    Public Structure SYSTEMTIME
        Dim wYear As Short
        Dim wMonth As Short
        Dim wDayOfWeek As Short
        Dim wDay As Short
        Dim wHour As Short
        Dim wMinute As Short
        Dim wSecond As Short
        Dim wMilliseconds As Short
    End Structure
    'TODO: Move DisplayQuotes() to modQuotes, change all ctrl refs to prepend "frmQuotes."
    'todo: run most of this at startup to fill in the fsQuotes global
    'todo: Make most of this into several separate routines; GetStartupQuotesFile, GetDefaultQuotesFile
    'todo:   DetermineQuoteFileType, GetQuoteCount(filespec), GetRandomQuote(n), GetNthQuote(ln1,ln2,posn)
    'todo:   DisplayAQuote(qn,qmax,ln1,ln2) so they can be called from things like Find, FindNext,
    'todo:   get quote by number, etc.
    'todo: Also make the open and other routines use (maybe leave open?) the current quote file, but
    'todo:  remember to close and/or rewind it when you paste a quote or open a new quote file
    'todo: Implement Open... and rename Open quotation file to "Edit current quote database" or "Edit..."

    'todo:  Support .txt or .quo/.qte/.fc/.cke files on command line.  Also possibly a settings file
    'todo:  type that is .ini formatted and can specify a file name, or other default settings such
    'todo:  as color, font, etc, etc.  Allows for several files (only one of each type) on the cmd line, too.

    Sub DisplayQuote()

        Dim strline1 As String = ""
        Dim strline2 As String = ""
        Dim qn As Integer
        Dim qmax As Integer

        GetStartupQuoteFile()
        DetermineQuoteFileType()

        qmax = CountQuotesInFile()
        If qmax > 0 Then
            qn = 0
            Do Until qn <> 0
                qn = GetRandomQuoteNumber(qmax)
            Loop

            GetSelectedQuoteFromFile(qn, strline1, strline2) 'todo: specifify file in param?
            DisplayQuoteInWindow(Me, qn, qmax, strline1, strline2)
        Else
            MsgBox("No quotes present in file!" & vbCrLf & vbCrLf & fsQuotes, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly)
            End ' Program
        End If

    End Sub 'DisplayQuote



    'UPGRADE_WARNING: Form event frmQuote.Activate has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
    'Private Sub frmQuote_Activated(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Activated
    'WheelHook(Me) ' active mouse scroll wheel support code for this form
    'End Sub

    'UPGRADE_WARNING: Form event frmQuote.Deactivate has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
    'Private Sub frmQuote_Deactivate(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Deactivate
    'WheelUnHook() ' DEactive mouse scroll wheel support code for this form
    'End Sub

    Private Sub FrmQuote_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
        Select Case KeyAscii
            Case 27 'escape
                Me.Close()
            Case System.Windows.Forms.Keys.Tab
                Call MnuFileNextNumericQuote_Click(mnuFileNextNumericQuote, New System.EventArgs())
            Case Else
                'ignore it
        End Select

        eventArgs.KeyChar = Chr(KeyAscii)
        If KeyAscii = 0 Then
            eventArgs.Handled = True
        End If
    End Sub

    Private Sub FrmQuote_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.MouseEventArgs) Handles MyBase.MouseUp
        Dim Button As Short = eventArgs.Button \ &H100000
        Dim Shift As Short = System.Windows.Forms.Control.ModifierKeys \ &H10000
        Dim X As Single = VB6.PixelsToTwipsX(eventArgs.X)
        Dim Y As Single = VB6.PixelsToTwipsY(eventArgs.Y)

        Select Case Button

            Case 1 ' Left Click
                ' will never get called; lbl*_Click methods supercede this (apparently)
            Case 2 ' Right Click
                'TODO: Add right-click menu support back.
                'UPGRADE_ISSUE: Form method frmQuote.PopupMenu was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
                'PopupMenu(mnuFormRightClick)
            Case 4 ' Middle click (scroll wheel button)
                DisplayQuote()
            Case Else
                DisplayQuote()
        End Select

    End Sub

    'TODO: UPGRADE_ISSUE: Form event Form.DragDrop was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="ABD9AF39-7E24-4AFF-AD8D-3675C1AA3054"'
    Private Sub Form_DragDrop(ByRef Source As System.Windows.Forms.Control, ByRef X As Single, ByRef Y As Single)
        'todo: support drag and drop of text files to import into database.
        'TODO: UPGRADE_WARNING: TypeOf has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
        If TypeOf Source Is System.Windows.Forms.TextBox Then
            'todo: replace the destruction of the clipboard with a call to a routine
            ' to put a string into the database.  Call that same routine from
            ' mnuEditPaste_Click
            My.Computer.Clipboard.SetText(Source.Text) ' may need to deal with RichText, DDE, whatnot
            MnuEditPaste_Click(mnuEditPaste, New System.EventArgs())
        Else
            Beep()
            MsgBox("Incorrect data type; can only drag-and-drop text into Quotes window.", MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation, "Quotes")
        End If

    End Sub

    Private Sub FrmQuote_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Dim KeyCode As Short = eventArgs.KeyCode
        Dim Shift As Short = eventArgs.KeyData \ &H10000

        Dim AltDown As Short
        Dim ShiftDown As Short
        Dim ctrldown As Short

        AltDown = (Shift And VB6.ShiftConstants.AltMask) > 0
        ShiftDown = (Shift And VB6.ShiftConstants.ShiftMask) > 0
        ctrldown = (Shift And VB6.ShiftConstants.CtrlMask) > 0

        Select Case KeyCode

            'TODO: See if the tab key code not working is still true in VB.NET like it ws in VB6
            'This code for the Tab key doesn't appear to work, do it in KeyPress instead
            '        Case vbKeyTab
            '            If (Not AltDown) And (Not ShiftDown) Then
            '                MsgBox "tab"
            '                 Call mnuFileNextNumericQuote_Click
            '            End If
            '
            '            If (ShiftDown) Then
            '                MsgBox "shift tab"
            '                 Call mnuFilePreviousNumericQuote_Click
            '            End If

            Case System.Windows.Forms.Keys.F3
                ' "find next" in most Windows apps
                If (Not ShiftDown) Then
                    FindNextQuote()
                End If

            Case System.Windows.Forms.Keys.F5
                ' refresh quote or refresh window

                If (ShiftDown) Then
                    Me.Refresh()
                Else
                    DisplayQuote()
                End If

            Case 32 ' space
                DisplayQuote()

            Case Asc("Q")
                If AltDown Then
                    End 'end the program on alt+q as well as ctrl+q
                    'todo: Use me.unload or frmquote.unload instead? Which is better and why? 
                End If

                'todo: implement other alts that don't conflict with alt+ltr for menus

                'TODO: See if the following "leak through" is still true in VB.NET like it was in VB6
                'DO NOT USE 13 AND 27, THEY "LEAK THROUGH" FROM HITTING
                ' RETURN FOR OK/default and ESC for CANCEL in MSGBOXes
                'Case 13 ' Return or Enter
                '    DisplayQuote
                'Case 27 'esc
                '    End ' program

            Case 33 And (Shift = 0) ' PgUp - must check for shift=0
                'display previous numeric quote
                Call MnuFilePreviousNumericQuote_Click(mnuFilePreviousNumericQuote, New System.EventArgs())

            Case 34 And (Shift = 0) ' PgDn or
                'display next numeric quote
                Call MnuFileNextNumericQuote_Click(mnuFileNextNumericQuote, New System.EventArgs())

            Case 35 And (Shift = 0) ' End
                'display last numeric quote
                Call MnuFileLastNumericQuote_Click(mnuFileLastNumericQuote, New System.EventArgs())

            Case 36 And (Shift = 0) ' Home
                'display quote #1 (first numeric quote)
                Call MnuFileFirstNumericQuote_Click(mnuFileFirstNumericQuote, New System.EventArgs())

            Case 37 And (Shift = 0) ' Left Arrow
                'display previous numeric quote
                Call MnuFilePreviousNumericQuote_Click(mnuFilePreviousNumericQuote, New System.EventArgs())

            Case 38 And (Shift = 0) ' Up Arrow
                'display quote #1 (first numeric quote)
                Call MnuFileFirstNumericQuote_Click(mnuFileFirstNumericQuote, New System.EventArgs())

            Case 39 And (Shift = 0) ' Right Arrow
                ' display next numeric quote
                Call MnuFileNextNumericQuote_Click(mnuFileNextNumericQuote, New System.EventArgs())

            Case 40 And (Shift = 0) ' Down Arrow
                'display last numeric quote
                Call MnuFileLastNumericQuote_Click(mnuFileLastNumericQuote, New System.EventArgs())

            Case 45 And (Shift = 0) ' Insert (Paste)
                MnuEditPaste_Click(mnuEditPaste, New System.EventArgs())

            Case 46 And (Shift = 0) ' Delete (Cut)
                CopyQuoteToClipboard()

            Case Else
                ' do nothing
                'MsgBox "Key code:" + Str(KeyCode) + " - " + Chr(KeyCode) + vbCrLf _
                '+ "Modifier codes:" + Str(Shift)
        End Select



    End Sub

    Private Sub FrmQuote_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Me.Show()
        DisplayQuote()
    End Sub


    Private Sub LblAttribution_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lblAttribution.Click
        'DisplayQuote
        'TODO: display next quote by that person (if available) or display Wikipedia info on the person?
    End Sub

    Private Sub LblAttribution_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.MouseEventArgs) Handles lblAttribution.MouseUp
        Dim Button As Short = eventArgs.Button \ &H100000
        Dim Shift As Short = System.Windows.Forms.Control.ModifierKeys \ &H10000
        Dim X As Single = VB6.PixelsToTwipsX(eventArgs.X)
        Dim Y As Single = VB6.PixelsToTwipsY(eventArgs.Y)
        Select Case Button
            Case 1 ' left-click
                DisplayQuote()
            Case 2 ' right-click
                'eventually add a context menu with Copy, Paste, and
                '  "Find...", "Find Next", and "Find 'current attrib text'"
                ' for now, just do the "Find 'current attrib text' without a menu
                gstrLastFindString = lblAttribution.Text
                glngLastFindPos = 1
                FindQuotePrompt() 'or FindNextQuote()  ?
            Case 4 ' scroll-click
                'do nothing
            Case Else
                ' do nothing
        End Select

    End Sub

    'TODO: UPGRADE_ISSUE: Label event lblAttribution.DragDrop was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="ABD9AF39-7E24-4AFF-AD8D-3675C1AA3054"'
    Private Sub LblAttribution_DragDrop(ByRef Source As System.Windows.Forms.Control, ByRef X As Single, ByRef Y As Single)
        'todo: support drag and drop of text files to import into database
    End Sub

    Private Sub LblQuote_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lblQuote.Click
        'TODO: Handle event in _MouseUp instead of _Click so we can have right-click/context/shortcut menu on right-click?
        'DisplayQuote
    End Sub

    Private Sub LblQuote_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lblQuote.DoubleClick
        'Ignore double-clicks; uncomment line (and/or change) the line below if you want to support them, though.
        'DisplayQuote
    End Sub

    'TODO: UPGRADE_ISSUE: Label event lblQuote.DragDrop was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="ABD9AF39-7E24-4AFF-AD8D-3675C1AA3054"'
    Private Sub LblQuote_DragDrop(ByRef Source As System.Windows.Forms.Control, ByRef X As Single, ByRef Y As Single)
        'todo: Support drag and drop of text files to import into database file
    End Sub

    Private Sub LblQuote_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.MouseEventArgs) Handles lblQuote.MouseUp
        Dim Button As Short = eventArgs.Button \ &H100000
        Dim Shift As Short = System.Windows.Forms.Control.ModifierKeys \ &H10000
        Dim X As Single = VB6.PixelsToTwipsX(eventArgs.X)
        Dim Y As Single = VB6.PixelsToTwipsY(eventArgs.Y)
        Select Case Button
            Case 1 'left-click
                DisplayQuote()
            Case 2 'right-click
                'TODO: UPGRADE_ISSUE: Form method frmQuote.PopupMenu was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
                'PopupMenu(mnuFormRightClick)
            Case 4 ' scroll click
        End Select

    End Sub

    Public Sub MnuFileFirstNumericQuote_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuFileFirstNumericQuote.Click

        Dim lngMaxQuotes As Integer
        'Dim lngNextQuote As Integer
        Dim strline1 As String = ""
        Dim strline2 As String = ""

        lngMaxQuotes = CountQuotes(fsQuotes)

        If lngMaxQuotes > 0 Then
            GetSelectedQuoteFromFile(1, strline1, strline2)
            DisplayQuoteInWindow(Me, 1, lngMaxQuotes, strline1, strline2)
        Else
            Beep()
        End If

    End Sub


    Public Sub MnuFileLastNumericQuote_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuFileLastNumericQuote.Click
        Dim lngMaxQuotes As Integer
        'Dim lngNextQuote As Integer
        Dim strline1 As String = ""
        Dim strline2 As String = ""

        lngMaxQuotes = CountQuotes(fsQuotes)

        If lngMaxQuotes > 0 Then
            GetSelectedQuoteFromFile(lngMaxQuotes, strline1, strline2)
            DisplayQuoteInWindow(Me, lngMaxQuotes, lngMaxQuotes, strline1, strline2)
        Else
            Beep()
        End If

    End Sub

    Public Sub MnuFileNextNumericQuote_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuFileNextNumericQuote.Click

        Dim lngMaxQuotes As Integer
        Dim lngNextQuote As Integer
        Dim strline1 As String = ""
        Dim strline2 As String = ""

        lngMaxQuotes = CountQuotes(fsQuotes)

        lngNextQuote = glngLastQuote + 1

        If lngNextQuote > lngMaxQuotes Then
            lngNextQuote = 1
        End If

        GetSelectedQuoteFromFile(lngNextQuote, strline1, strline2)
        DisplayQuoteInWindow(Me, lngNextQuote, lngMaxQuotes, strline1, strline2)

    End Sub

    Public Sub MnuFilePreviousNumericQuote_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuFilePreviousNumericQuote.Click

        Dim lngMaxQuotes As Integer
        Dim lngPrevQuote As Integer
        Dim strline1 As String = ""
        Dim strline2 As String = ""

        lngMaxQuotes = CountQuotes(fsQuotes)

        lngPrevQuote = glngLastQuote - 1

        If lngPrevQuote < 1 Then
            lngPrevQuote = lngMaxQuotes
        End If

        GetSelectedQuoteFromFile(lngPrevQuote, strline1, strline2)
        DisplayQuoteInWindow(Me, lngPrevQuote, lngMaxQuotes, strline1, strline2)

    End Sub

    Public Sub MnuFormRightClickCopy_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuFormRightClickCopy.Click
        MnuEditCut_Click(mnuEditCut, New System.EventArgs())
    End Sub

    Public Sub MnuFormRightClickPaste_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuFormRightClickPaste.Click
        MnuEditPaste_Click(mnuEditPaste, New System.EventArgs())
    End Sub


    Public Sub MnuEditCopy_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuEditCopy.Click
        CopyQuoteToClipboard()
    End Sub

    Private Sub CopyQuoteToClipboard()
        My.Computer.Clipboard.Clear()
        My.Computer.Clipboard.SetText(lblQuote.Text & vbCrLf & lblAttribution.Text)
    End Sub

    Public Sub MnuEditCut_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuEditCut.Click
        CopyQuoteToClipboard()
    End Sub

    Public Sub MnuEditPaste_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuEditPaste.Click

        Dim intButton As Short
        Dim fc As Short
        Dim fsBackup As String
        Dim qmax As Integer
        Dim strline1 As String = ""
        Dim strline2 As String = ""

        fsBackup = fsQuotes & "_backup"
        fc = FreeFile()

        intButton = MsgBox("Are you sure you want to add the quote:" & vbCrLf & vbCrLf & My.Computer.Clipboard.GetText & vbCrLf & vbCrLf & "to the quote database " & vbCrLf & vbCrLf & fsQuotes & " ?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Paste quotation into quote database?")

        'TODO: check for " at start, end of quote, add if necessary.  Watch out for attribution line, find "<whitespace>-<-><whitespace>Name and make sure there is a period/exclamation/questionmark and a quote at the end of the line before.

        Select Case intButton

            Case MsgBoxResult.Yes
                On Error Resume Next
                Kill(fsBackup) ' delete any previous backups
                On Error GoTo ErrHandler
                FileCopy(fsQuotes, fsBackup) ' backup the current database before appending

                ' append the quote to the end of the current database
                FileOpen(fc, fsQuotes, OpenMode.Append)
                PrintLine(fc, "") 'todo: get file type from a global variable, use % instead of "" if %-delimited quote db
                PrintLine(fc, My.Computer.Clipboard.GetText)
                PrintLine(fc, "") 'todo: get file type from a global variable, use % instead of "" if %-delimited quote db
                'MsgBox "Quotation has been added to the database.  Original database copied " _
                '& " to file " + vbCrLf + vbCrLf + fsBackup, _
                'vbInformation + vbOKOnly, _
                '"Paste quotation results"

                FileClose(fc)
                ' now display quote we just pasted

                qmax = CountQuotes(fsQuotes)
                GetSelectedQuoteFromFile(qmax, strline1, strline2)
                DisplayQuoteInWindow(Me, qmax, qmax, strline1, strline2)


            Case MsgBoxResult.No
                ' do nothing

            Case Else
                MsgBox("Unknown button pressed when pasting quote - contact programmer.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Quotes ERROR!")

        End Select

Exxit:

        On Error Resume Next
        FileClose(fc) 'just to make sure
        Exit Sub

ErrHandler:

        MsgBox(CDbl("Unexpected error pasting quotation: #") + Err.Number + CDbl(" - ") + CDbl(Err.Description), MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Quotation paste ERROR!")
        Resume Exxit

    End Sub 'mnuEditPaste_Click

    Public Sub MnuEditPreferences_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuEditPreferences.Click
        MsgBox("Feature not yet implemented.")
    End Sub

    Public Sub MnuFileByNumber_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuFileByNumber.Click

        Dim qn As Integer
        Dim qmax As Integer 'todo: move to global
        Dim strQuoteNumber As String
        Dim strline1 As String = ""
        Dim strline2 As String = ""

        On Error GoTo ErrHandler

        qmax = CountQuotes(fsQuotes)

        If glngLastQuote = 0 Then
            glngLastQuote = qmax
        End If

        strQuoteNumber = InputBox("Enter quote number to display 1..." & Str(qmax), "Display quote by number", CStr(glngLastQuote))

        If strQuoteNumber = "" Then Exit Sub

        qn = Val(strQuoteNumber)

        Select Case qn
            Case Is < 0
                MsgBox("Number must be in range 1 to " & Str(qmax), MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Input error")


            Case 1 To qmax

                GetSelectedQuoteFromFile(qn, strline1, strline2)
                DisplayQuoteInWindow(Me, qn, qmax, strline1, strline2)

            Case Is > qmax
                MsgBox("Number must be in range 1 to " & Str(qmax), MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Input error")

            Case Else
                Exit Sub
        End Select

Exxit:
        Exit Sub

ErrHandler:
        Beep()
        Resume Exxit


    End Sub

    Public Sub MnuFileEdit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuFileEdit.Click
        Shell("Notepad " & fsQuotes, AppWinStyle.NormalFocus)
    End Sub

    'todo: move find, findnext, by number to edit menu.

    Public Sub MnuFileFind_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuFileFind.Click

        FindQuotePrompt()

    End Sub

    Private Sub FindQuotePrompt()
        Dim strToFind As String

        strToFind = InputBox("Enter text for which to find a quote:", "Find quote", gstrLastFindString)

        If strToFind = "" Then Exit Sub

        'remember search string for next time
        gstrLastFindString = strToFind
        glngLastFindPos = 1 ' 1=start at first byte of file

        'todo: dim "Find Next" menu item until after first successfuly find
        FindNextQuote()

    End Sub

    Private Sub FindNextQuote()

        Dim strline1 As String = ""
        Dim strline2 As String = ""
        Dim qn As Integer
        Dim qmax As Integer
        Dim lngResult As Integer

        If gstrLastFindString = "" Then
            Beep()
            'FindQuotePrompt
            Exit Sub
        End If

        lngResult = FindQuote(gstrLastFindString, glngLastFindPos, strline1, strline2, qn)
        'glngLastFindPos is retained across searches, to implement Find Next

        Select Case lngResult
            Case 0
                ' display the quote found and returned in strLine1 and strLine2
                qmax = CountQuotes(fsQuotes) 'todo: retain qmax globally, as lngNumQuotesInFile
                DisplayQuoteInWindow(Me, qn, qmax, strline1, strline2)
            Case 1
                ' nothing found
                MsgBox("No quote was found containing:" & vbCrLf & vbCrLf & gstrLastFindString, MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly, "Find failed!")
                glngLastFindPos = 1
            Case Else
                ' error
                MsgBox("Unexpected error searching for quote: #" & Str(lngResult) & vbCrLf & ErrorToString(lngResult), MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Find error!")
        End Select

        ' open file, use seek #fc, gstrlastsearchpos+1 to position, continu searching
        ' for gstrLastFind string.
        'lngResult = FindQuote(gstrLastFind, strLine1, strLine2, gstrLastSearchPos)
        ' then display if lngresult<>0
        ' and remember last search position; return in param from FindQuote?

    End Sub

    Public Sub MnuFileFindNext_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuFileFindNext.Click

        FindNextQuote()

    End Sub

    'UPGRADE_WARNING: Event frmQuote.Resize may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub FrmQuote_Resize(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Resize
        'TODO:  Remember window size in Quotes.Ini on quit.
        On Error Resume Next
        lblAttribution.SetBounds(VB6.TwipsToPixelsX(100), VB6.TwipsToPixelsY((VB6.PixelsToTwipsY(ClientRectangle.Height) - VB6.PixelsToTwipsY(lblAttribution.Height) - 100)), VB6.TwipsToPixelsX(VB6.PixelsToTwipsX(ClientRectangle.Width) - 200), lblAttribution.Height)
        lblQuote.SetBounds(VB6.TwipsToPixelsX(100), VB6.TwipsToPixelsY(100), VB6.TwipsToPixelsX(VB6.PixelsToTwipsX(ClientRectangle.Width) - 200), VB6.TwipsToPixelsY(VB6.PixelsToTwipsY(lblAttribution.Top) - VB6.PixelsToTwipsY(lblQuote.Top)))

    End Sub

    Public Sub MnuFileNew_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuFileNew.Click
        'New quote file creation
        MsgBox("Feature not yet implemented")
    End Sub


    Public Sub MnuFileOpen_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuFileOpen.Click
        MsgBox("Feature not yet implemented.")
    End Sub


    Public Sub MnuFileQuit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuFileQuit.Click
        End 'program
    End Sub


    Public Sub MnuFileRandomQuote_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuFileRandomQuote.Click
        DisplayQuote()
    End Sub

    Public Sub MnuHelpAbout_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuHelpAbout.Click
        MsgBox(My.Application.Info.AssemblyName & vbCrLf & vbCrLf _
             & My.Application.Info.Description & vbCrLf & vbCrLf _
             & My.Application.Info.CompanyName & vbCrLf & vbCrLf _
             & My.Application.Info.Copyright & Format(Now, "yyyy") & vbCrLf & vbCrLf _
             & "Version " & My.Application.Info.Version.Major & "." & My.Application.Info.Version.Minor & "." & My.Application.Info.Version.Revision,
             MsgBoxStyle.Information + MsgBoxStyle.OkOnly)

    End Sub

    Private Sub Timer1_Tick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Timer1.Tick
        DisplayQuote() ' every so many seconds (30 secs as of this writing)

        ' todo: Add a "Preferences" dialog where they can set this interval,
        ' todo:   and the font used, etc, etc.  Store in Quotes.Ini in App.Path
        ' todo:    perhaps also the max/min font size, the fontsize stepdown interval, etc.
    End Sub

    'UPGRADE_NOTE: MouseWheel was upgraded to MouseWheel_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    'TODO: This worked in VB6, but not in VB.NET; find replacement for reacting to mouse wheel rotation. 
    'Public Sub MouseWheel_Renamed(ByVal MouseKeys As Integer, ByVal Rotation As Integer, ByVal Xpos As Integer, ByVal Ypos As Integer)
    'Dim NewValue As Integer
    '
    '   On Error Resume Next
    '
    '   If Rotation > 0 Then
    '   Call mnuFilePreviousNumericQuote_Click(mnuFilePreviousNumericQuote, New System.EventArgs())
    '   Else
    '   Call mnuFileNextNumericQuote_Click(mnuFileNextNumericQuote, New System.EventArgs())
    '   End If
    '
    '  End Sub
    '
    Private Sub FrmQuote_MouseWheel(sender As Object, e As MouseEventArgs) Handles Me.MouseWheel
        'Debug.Print("MouseWheel e.delta:" + Str(e.Delta))
        Try
            If e.Delta >= 120 Then
                Call MnuFilePreviousNumericQuote_Click(mnuFilePreviousNumericQuote, New System.EventArgs())
            ElseIf e.Delta <= -120 Then
                Call MnuFileNextNumericQuote_Click(mnuFileNextNumericQuote, New System.EventArgs())
            End If
        Catch ex As Exception
            ' ignore errors
        End Try


    End Sub


    'TODO: Routines  below copied from modQuotes.bas module in VB6 into frmMain.vb in VB.NET to get things to work.  Some may need replacement.

    'UPGRADE_WARNING: Structure SYSTEMTIME may require marshalling attributes to be passed as an argument in this Declare statement. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
    Public Declare Sub GetLocalTime Lib "kernel32" (ByRef lpSystemTime As SYSTEMTIME)

    'UPGRADE_WARNING: Structure SYSTEMTIME may require marshalling attributes to be passed as an argument in this Declare statement. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
    Public Declare Sub GetSystemTime Lib "kernel32" (ByRef lpSystemTime As SYSTEMTIME)

    'todo: use to seed Rnd or to get a random # from 0-999
    Function GetSystemMilliseconds() As Integer

        Dim sdtSystemTime As SYSTEMTIME

        GetSystemTime(sdtSystemTime)

        GetSystemMilliseconds = CInt(sdtSystemTime.wMilliseconds)

    End Function


    Public Function CountQuotes(ByRef fs As String) As Integer

        Dim qc As Integer
        Dim fcQuotes As Short
        Dim strLine As String
        Dim strFirstChar As String

        fcQuotes = FreeFile()

        FileOpen(fcQuotes, fs, OpenMode.Input)

        qc = 0

        Do Until EOF(fcQuotes)

            strLine = LineInput(fcQuotes)

            strFirstChar = Strings.Left(Trim(strLine), 1)

            If (strFirstChar = """") Or (strFirstChar = "%") Or ((strFirstChar <> "-") And (strFirstChar <> "")) Then
                'todo: add support for blanklines, Access MDB files, other databases via ODBC,JDBC,whatever
                qc = qc + 1
            End If

        Loop

        FileClose(fcQuotes)

        CountQuotes = qc

    End Function 'CountQuotes

    Public Function TrimWhitespace(ByRef strToCollapse As String) As String

        Dim strToReturn As String = ""
        Dim i As Short
        Dim strChar As String

        strToCollapse = Trim(strToCollapse)

        For i = 1 To Len(strToCollapse)
            strChar = Mid(strToCollapse, i, 1)

            If Asc(strChar) >= 32 Then
                strToReturn = strToReturn + strChar
            Else
                ' it's a control character, strip it (by not adding it to the strToReturn)
            End If
        Next i

        TrimWhitespace = strToReturn

    End Function 'TrimWhitespace

    Public Sub GetStartupQuoteFile()

        'TODO: Test VB.Command() and Dir() modifications for VB.NET vs VB6 further
        ' In VB.NET use Command() or  My.Application.CommandLineArgs instead of VB.Command()

        'If VB.Command() <> "" Then
        Dim strCommandLine As String
        strCommandLine = Command() 'TODO: or use  My.Application.CommandLineArgs
        Debug.Print("Command() returned: " & Command())
        Debug.Print("My.Application.CommandLineArgs returned " + My.Application.CommandLineArgs.ToString())
        Debug.Print("Dir(Command()) returned:" & Dir(Command()))
        'Debug.Print("My.Computer.FileSystem.GetDirectoryInfo(Command()) returned " & My.Computer.FileSystem.GetDirectoryInfo(Command()).ToString)


        If strCommandLine <> "" Then
            'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
            'If Dir(VB.Command(), FileAttribute.Normal + FileAttribute.ReadOnly) <> "" Then
            'TODO: figure out how to add equivalent of FileAttribute.Normal + FileAttribute.ReadOnly
            Dim strPath As String = Dir(Command())
            'Dim strPath As String = My.Computer.FileSystem.GetDirectoryInfo(strCommandLine).ToString
            If strPath <> "" Then
                fsQuotes = strPath
            End If
        End If

        ' if no cmd line args or file named on cmd line does not exist,
        ' use default filespec and path (file named same as .exe except
        ' ends in .txt, and in the same dir and the .exe)

        If fsQuotes = "" Then
            fsQuotes = GetDefaultQuoteFile()
        End If
    End Sub

    Public Function GetDefaultQuoteFile() As String

        Dim strDefaultPath As String = My.Application.Info.DirectoryPath & "\" & My.Application.Info.AssemblyName & ".txt"
        Debug.Print("Using default quote file:" & strDefaultPath)

        'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
        'todo: changed to using My.Computer.FileSystem.GetDirectoryInfo method instead of Dir
        Dim strPath2 As String = Dir(strDefaultPath, FileAttribute.Normal + FileAttribute.ReadOnly)
        Debug.Print("Default quote file path after using Dir():" & strPath2)

        If Dir(strDefaultPath, FileAttribute.Normal + FileAttribute.ReadOnly) = "" Then
            MsgBox("No quotes file found!" & vbCrLf & vbCrLf & fsQuotes, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly)
            End ' program
        End If

        GetDefaultQuoteFile = strDefaultPath

    End Function

    Public Sub DetermineQuoteFileType()

        ' determine % delimited multiline quote file or " delimited single-line quote

        Dim fcQuotes As Short
        Dim strLine As String

        gIntFileType = cintUnknownDelimited

        fcQuotes = FreeFile()

        FileOpen(fcQuotes, fsQuotes, OpenMode.Input)

        Do Until EOF(fcQuotes) Or (gIntFileType <> cintUnknownDelimited)

            strLine = LineInput(fcQuotes)

            Select Case Strings.Left(Trim(strLine), 1)

                Case "%"
                    If Trim(strLine) = "%" Then
                        gIntFileType = cintPercentDelimited ' percent delimited quote file
                    Else
                        ' line that starts with % but has other non-whitespace text after
                    End If

                Case """"
                    gIntFileType = cintQuoteMarkDelimited ' " delimited strings with 2nd line for attrib

                Case ""
                    'todo: could have blank-line delimited quotes,
                    ' gintfiletype = cintBlankLineDelimited ' blank-line delimited quotes

                    ' ignore line and keep looking for first recogninzable delimiter

                Case Else
                    gIntFileType = cintQuoteMarkDelimited

                    'todo: add support for blanklines, Access MDB files, other databases via ODBC,JDBC,whatever

            End Select

        Loop

        FileClose(fcQuotes)
        'todo: is there some sort of "rewind" in VB?; find out and use if so

    End Sub

    Public Function CountQuotesInFile() As Integer
        Dim qmax As Integer

        qmax = CountQuotes(fsQuotes) 'this is pretty fast, but...

        If gIntFileType = cintPercentDelimited Then qmax = qmax - 1 '(are n quotes in a %-delimited file, but n+1 %'s)

        CountQuotesInFile = qmax

    End Function 'CountQuotesInFile

    Function GetRandomQuoteNumber(ByRef qmax As Integer) As Integer

        If qmax = 0 Then
            GetRandomQuoteNumber = (Int(DatePart(Microsoft.VisualBasic.DateInterval.Second, Now) + DatePart(Microsoft.VisualBasic.DateInterval.Minute, Now) + DatePart(Microsoft.VisualBasic.DateInterval.Hour, Now)) + Int(Rnd() * DatePart(Microsoft.VisualBasic.DateInterval.Second, Now)))
        Else
            GetRandomQuoteNumber = System.Math.Abs(Int(Rnd() * System.Math.Abs(qmax)) + 1 - Int(DatePart(Microsoft.VisualBasic.DateInterval.Second, Now) + DatePart(Microsoft.VisualBasic.DateInterval.Minute, Now) + DatePart(Microsoft.VisualBasic.DateInterval.Hour, Now)))
        End If
    End Function


    Public Sub GetSelectedQuoteFromFile(ByRef qn As Integer, ByRef strline1 As String, ByRef strline2 As String)

        Dim qc As Integer
        Dim strLine As String
        Dim fcQuotes As Short
        Dim strFirstChar As String

        fcQuotes = FreeFile()

        FileOpen(fcQuotes, fsQuotes, OpenMode.Input)

        qc = 0
        strline1 = ""
        strline2 = ""

        Do Until EOF(fcQuotes) Or (qc = qn)

            strLine = LineInput(fcQuotes)

            Select Case gIntFileType

                Case cintPercentDelimited
                    If Strings.Left(Trim(strLine), 1) = "%" Then
                        ' found start of quote
                        qc = qc + 1
                        If qc = qn Then
                            ' read lines until next %, add to strLine1
                            strLine = LineInput(fcQuotes)
                            Do Until EOF(fcQuotes) Or Strings.Left(Trim(strLine), 1) = "%"
                                strline1 = strline1 & vbCrLf & TrimWhitespace(strLine)
                                strLine = LineInput(fcQuotes)
                            Loop
                        Else
                            ' ignore lines until next % and check qc=qn again
                        End If
                    Else
                        ' not started with %, we're somewhere in mid-quote that we're skipping
                    End If

                Case cintQuoteMarkDelimited
                    strFirstChar = Strings.Left(Trim(strLine), 1)
                    If strFirstChar = """" Or ((strFirstChar <> "-") And (strFirstChar <> "")) Then
                        qc = qc + 1
                        If qc = qn Then
                            strline1 = strLine
                            On Error Resume Next
                            strline2 = LineInput(fcQuotes)
                        End If
                    Else
                        ' not start of a quote, keep looking
                    End If

                    'Case cintQuoteMarkDelimited
                    ' todo: eventually add support for blank-line delimited quotes.

                Case Else
                    ' not start of a quote, but somewhere in mid-quote that we're skipping.

                    'todo: add support for blanklines, Access MDB files, other databases via ODBC,JDBC,whatever

            End Select

        Loop

        FileClose(fcQuotes)


    End Sub


    Public Sub DisplayQuoteInWindow(ByRef frm As System.Windows.Forms.Form, ByRef qc As Integer, ByRef qmax As Integer, ByRef strline1 As String, ByRef strline2 As String)

        'Turn off timer that displays a new quote every few seconds
        Timer1.Enabled = False

        If strline1 = "" Then
            strline1 = "No quotes found - file empty."
            strline2 = fsQuotes
        End If

        If strline2 = "" Then
            Select Case gIntFileType
                Case cintPercentDelimited
                    ' leave blank
                Case cintQuoteMarkDelimited
                    strline2 = "  - Unknown"
                Case Else
                    ' leave blank
            End Select
        End If

        frm.Text = "Quote #" & Str(qc) & "/" & Str(qmax)

        ' lblQuote.ToolTipText = Str(Len(strline1)) & " characters"
        ' lblAttribution.ToolTipText = Str(Len(strline2)) & " characters"
        ToolTip1.SetToolTip(lblQuote, Str(Len(strline1)) & " characters")
        ToolTip1.SetToolTip(lblAttribution, Str(Len(strline2)) & " characters")

        'Check length of text, if > a certain size, reduce font size of lblQuote OR change bottom right corner of window to be taller/wider or both.

        'todo: Find out how to tell if text of a certain font/size will fit into a specific rectangle, keep sizing up until it fits or gets beyond size of display
        'todo: Implement scroll bar if gets to big?  I'd rather not...ugly.
        'todo: Resize lblattribution separately from lblQuote (separate if/select block)
        'todo: Drop by 2 point sizes for every 100 chars (or something) chars in case of attribution.
        'todo: Check width/height of lblQuote and resize frmQuote to match

        strline1 = TrimWhitespace(strline1)

        If gIntFileType = cintQuoteMarkDelimited Then
            If Len(strline1) < 160 Then
                strline1 = vbCrLf & strline1
            End If
        End If


        'TODO Figure out .NET AutoSize option for lblQuote and lblAttribution
        'TODO See also .AutoEllipsis and .BorderStyle and .ImageList/.ImageIndex/.ImageAlign and .PreferredWidth/Height and .BackgroundImage*


        Select Case Len(strline1)
            Case Is <= 50
                'what a PITA, can't just do: lblQuote.Font.Size = 20, much less lblQuote.size = 20 as in VB6; attribut is "read only" this way (DUMB!) have to do: 
                lblQuote.Font = New Font(lblQuote.Font.FontFamily, 36, lblQuote.Font.Style)
            Case Is <= 100
                lblQuote.Font = New Font(lblQuote.Font.FontFamily, 32, lblQuote.Font.Style)
            Case Is <= 150
                lblQuote.Font = New Font(lblQuote.Font.FontFamily, 24, lblQuote.Font.Style)
            Case Is <= 200
                lblQuote.Font = New Font(lblQuote.Font.FontFamily, 20, lblQuote.Font.Style)
            Case Is <= 250
                lblQuote.Font = New Font(lblQuote.Font.FontFamily, 18, lblQuote.Font.Style)
            Case Is <= 300
                lblQuote.Font = New Font(lblQuote.Font.FontFamily, 14, lblQuote.Font.Style)
            Case Is <= 350
                lblQuote.Font = New Font(lblQuote.Font.FontFamily, 14, lblQuote.Font.Style)
            Case Is <= 400
                lblQuote.Font = New Font(lblQuote.Font.FontFamily, 14, lblQuote.Font.Style)
            Case Is <= 500
                lblQuote.Font = New Font(lblQuote.Font.FontFamily, 14, lblQuote.Font.Style)
            Case Is <= 600
                lblQuote.Font = New Font(lblQuote.Font.FontFamily, 14, lblQuote.Font.Style)
            Case Is <= 700
                lblQuote.Font = New Font(lblQuote.Font.FontFamily, 12, lblQuote.Font.Style)
            Case Else  ' >700
                lblQuote.Font = New Font(lblQuote.Font.FontFamily, 10, lblQuote.Font.Style)
        End Select

        strline2 = strline2 & "    "

        Select Case Len(strline2)
            Case Is < 30
                lblAttribution.Font = New Font(lblAttribution.Font.FontFamily, 18, lblAttribution.Font.Style)
            Case Is < 40
                lblAttribution.Font = New Font(lblAttribution.Font.FontFamily, 18, lblAttribution.Font.Style)
            Case Is < 50
                lblAttribution.Font = New Font(lblAttribution.Font.FontFamily, 16, lblAttribution.Font.Style)
            Case Is < 60
                lblAttribution.Font = New Font(lblAttribution.Font.FontFamily, 14, lblAttribution.Font.Style)
            Case Is < 70
                lblAttribution.Font = New Font(lblAttribution.Font.FontFamily, 14, lblAttribution.Font.Style)
            Case Is < 80
                lblAttribution.Font = New Font(lblAttribution.Font.FontFamily, 12, lblAttribution.Font.Style)
            Case Is < 90
                lblAttribution.Font = New Font(lblAttribution.Font.FontFamily, 10, lblAttribution.Font.Style)
            Case Is < 110
                lblAttribution.Font = New Font(lblAttribution.Font.FontFamily, 10, lblAttribution.Font.Style)
            Case Else
                lblAttribution.Font = New Font(lblAttribution.Font.FontFamily, 10, lblAttribution.Font.Style)

        End Select

        If lblAttribution.Font.Size > lblQuote.Font.Size Then
            'was lblAttribution.Font.Size = lblQuote.Font.Size
            lblAttribution.Font = New Font(lblAttribution.Font.FontFamily, lblQuote.Font.Size, lblAttribution.Font.Style)
        End If

        ' Change any \ in quote to CRLF to allow multiline quotes to be encoded on a single line of text.

        strline1 = Replace(strline1, "\", vbCrLf)

        ' display the quote and attribution

        lblQuote.Text = strline1
        lblAttribution.Text = strline2

        frm.Refresh()

        ' restart the timer to display a new random quote every so often
        Timer1.Enabled = True
        glngLastQuote = qc

    End Sub

    Public Function FindQuote(ByRef strToFind As String, ByRef lngStartLoc As Integer, ByRef strline1 As String, ByRef strline2 As String, ByRef lngQuoteNumber As Integer) As Integer

        'todo: change error handler to Try/Catch
        On Error GoTo ErrHandler

        Dim qc As Integer = 0
        Dim fcQuotes As Short
        Dim blnMatchFound As Boolean = False
        Dim strFirstChar As String
        Dim strLine As String = ""
        strline1 = "" ' set function parameter default
        strline2 = "" ' set function parameter default

        fcQuotes = FreeFile()
        FileOpen(fcQuotes, fsQuotes, OpenMode.Input)

        ' Deal with a "Find Next"
        If lngStartLoc > 1 Then
            Seek(fcQuotes, lngStartLoc + 1)
        End If

        Do Until EOF(fcQuotes) Or blnMatchFound

            strLine = LineInput(fcQuotes)

            Select Case gIntFileType

                Case cintPercentDelimited

                    If Strings.Left(Trim(strLine), 1) = "%" Then
                        ' found start of quote
                        qc = qc + 1
                        ' read lines until next %, add to strLine1
                        strLine = LineInput(fcQuotes)
                        Do Until EOF(fcQuotes) Or Strings.Left(Trim(strLine), 1) = "%"
                            strline1 = strline1 & vbCrLf & TrimWhitespace(strLine)
                            strLine = LineInput(fcQuotes)
                        Loop
                        If InStr(1, strline1, strToFind) <> 0 Then
                            ' we found a match
                            blnMatchFound = True
                            lngQuoteNumber = qc
                            lngStartLoc = Seek(fcQuotes) ' or maybe Loc() or Loc()*128
                        Else
                            ' we didn't
                        End If
                    End If

                Case cintQuoteMarkDelimited
                    strFirstChar = Strings.Left(Trim(strLine), 1)
                    If strFirstChar = """" Or ((strFirstChar <> "-") And (strFirstChar <> "")) Then
                        qc = qc + 1
                        strline1 = strLine
                        On Error Resume Next
                        strline2 = LineInput(fcQuotes)
                        Dim strEntireQuote As String = strline1 & strline2
                        If InStr(1, strEntireQuote, strToFind) <> 0 Then
                            ' we found it
                            blnMatchFound = True
                            lngQuoteNumber = qc
                            lngStartLoc = Seek(fcQuotes) ' or maybe Loc() or Loc()*128
                        Else
                            ' we didn't
                        End If
                    Else
                        ' not start of a quote, keep looking
                    End If

                    'Case cintQuoteMarkDelimited

                    ' todo: eventually add support for blank-line delimited quotes.

                Case Else

                    ' not start of a quote, but somewhere in mid-quote that we're skipping.

                    'todo: add support for blanklines, Access MDB files, other databases via ODBC,JDBC,whatever


            End Select

        Loop

        If blnMatchFound Then
            FindQuote = 0
        Else
            FindQuote = 1
        End If

        'todo: change error handler to Try/Catch
Exxit:
        On Error Resume Next
        FileClose(fcQuotes)
        Exit Function

ErrHandler:
        FindQuote = Err.Number
        Resume Exxit

    End Function 'FindQuote

End Class