<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class FrmQuote
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents mnuFileNew As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFileOpen As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFileEdit As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFilePrint As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFileDivider1 As System.Windows.Forms.ToolStripSeparator
	Public WithEvents mnuFileFind As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFileFindNext As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFindOnInternet As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFileDivider2 As System.Windows.Forms.ToolStripSeparator
	Public WithEvents mnuFileRandomQuote As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFileByNumber As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFileFirstNumericQuote As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFilePreviousNumericQuote As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFileNextNumericQuote As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFileLastNumericQuote As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFileDivider3 As System.Windows.Forms.ToolStripSeparator
	Public WithEvents mnuFileQuit As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFile As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuEditUndo As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuEditBlank1 As System.Windows.Forms.ToolStripSeparator
	Public WithEvents mnuEditCut As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuEditCopy As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuEditPaste As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuEditDivider2 As System.Windows.Forms.ToolStripSeparator
	Public WithEvents mnuEditPreferences As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuEdit As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuHelpAbout As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuHelp As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFormRightClickCopy As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFormRightClickPaste As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFormRighClickDiv1 As System.Windows.Forms.ToolStripSeparator
	Public WithEvents mnuFormRightClickFind As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFormRightClickFindNext As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFormRightClickFindSelection As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFormRightClickFindAttribution As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFormRightClickFindNextAttribution As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFormRightClick As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents MainMenu1 As System.Windows.Forms.MenuStrip
	Public WithEvents Timer1 As System.Windows.Forms.Timer
	Public WithEvents lblAttribution As System.Windows.Forms.Label
	Public WithEvents lblQuote As System.Windows.Forms.Label
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmQuote))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.MainMenu1 = New System.Windows.Forms.MenuStrip()
        Me.mnuFile = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNew = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileOpen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileEdit = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFilePrint = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileDivider1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuFileFind = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileFindNext = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFindOnInternet = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileDivider2 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuFileRandomQuote = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileByNumber = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileFirstNumericQuote = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFilePreviousNumericQuote = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileNextNumericQuote = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileLastNumericQuote = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileDivider3 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuFileQuit = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuEdit = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuEditUndo = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuEditBlank1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuEditCut = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuEditCopy = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuEditPaste = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuEditDivider2 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuEditPreferences = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuHelp = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuHelpAbout = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFormRightClick = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFormRightClickCopy = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFormRightClickPaste = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFormRighClickDiv1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuFormRightClickFind = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFormRightClickFindNext = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFormRightClickFindSelection = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFormRightClickFindAttribution = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFormRightClickFindNextAttribution = New System.Windows.Forms.ToolStripMenuItem()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.lblAttribution = New System.Windows.Forms.Label()
        Me.lblQuote = New System.Windows.Forms.Label()
        Me.MainMenu1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MainMenu1
        '
        Me.MainMenu1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.MainMenu1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFile, Me.mnuEdit, Me.mnuHelp, Me.mnuFormRightClick})
        Me.MainMenu1.Location = New System.Drawing.Point(0, 0)
        Me.MainMenu1.Name = "MainMenu1"
        Me.MainMenu1.Size = New System.Drawing.Size(668, 24)
        Me.MainMenu1.TabIndex = 2
        '
        'mnuFile
        '
        Me.mnuFile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFileNew, Me.mnuFileOpen, Me.mnuFileEdit, Me.mnuFilePrint, Me.mnuFileDivider1, Me.mnuFileFind, Me.mnuFileFindNext, Me.mnuFindOnInternet, Me.mnuFileDivider2, Me.mnuFileRandomQuote, Me.mnuFileByNumber, Me.mnuFileFirstNumericQuote, Me.mnuFilePreviousNumericQuote, Me.mnuFileNextNumericQuote, Me.mnuFileLastNumericQuote, Me.mnuFileDivider3, Me.mnuFileQuit})
        Me.mnuFile.Name = "mnuFile"
        Me.mnuFile.Size = New System.Drawing.Size(37, 20)
        Me.mnuFile.Text = "&File"
        '
        'mnuFileNew
        '
        Me.mnuFileNew.Enabled = False
        Me.mnuFileNew.Name = "mnuFileNew"
        Me.mnuFileNew.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
        Me.mnuFileNew.Size = New System.Drawing.Size(259, 22)
        Me.mnuFileNew.Text = "&New quote file..."
        '
        'mnuFileOpen
        '
        Me.mnuFileOpen.Enabled = False
        Me.mnuFileOpen.Name = "mnuFileOpen"
        Me.mnuFileOpen.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.O), System.Windows.Forms.Keys)
        Me.mnuFileOpen.Size = New System.Drawing.Size(259, 22)
        Me.mnuFileOpen.Text = "&Open quote file..."
        '
        'mnuFileEdit
        '
        Me.mnuFileEdit.Name = "mnuFileEdit"
        Me.mnuFileEdit.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.E), System.Windows.Forms.Keys)
        Me.mnuFileEdit.Size = New System.Drawing.Size(259, 22)
        Me.mnuFileEdit.Text = "&Edit current quote file..."
        '
        'mnuFilePrint
        '
        Me.mnuFilePrint.Enabled = False
        Me.mnuFilePrint.Name = "mnuFilePrint"
        Me.mnuFilePrint.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.P), System.Windows.Forms.Keys)
        Me.mnuFilePrint.Size = New System.Drawing.Size(259, 22)
        Me.mnuFilePrint.Text = "Print quote..."
        '
        'mnuFileDivider1
        '
        Me.mnuFileDivider1.Name = "mnuFileDivider1"
        Me.mnuFileDivider1.Size = New System.Drawing.Size(256, 6)
        '
        'mnuFileFind
        '
        Me.mnuFileFind.Name = "mnuFileFind"
        Me.mnuFileFind.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.F), System.Windows.Forms.Keys)
        Me.mnuFileFind.Size = New System.Drawing.Size(259, 22)
        Me.mnuFileFind.Text = "&Find..."
        '
        'mnuFileFindNext
        '
        Me.mnuFileFindNext.Name = "mnuFileFindNext"
        Me.mnuFileFindNext.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.G), System.Windows.Forms.Keys)
        Me.mnuFileFindNext.Size = New System.Drawing.Size(259, 22)
        Me.mnuFileFindNext.Text = "Find Nex&t"
        '
        'mnuFindOnInternet
        '
        Me.mnuFindOnInternet.Enabled = False
        Me.mnuFindOnInternet.Name = "mnuFindOnInternet"
        Me.mnuFindOnInternet.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.I), System.Windows.Forms.Keys)
        Me.mnuFindOnInternet.Size = New System.Drawing.Size(259, 22)
        Me.mnuFindOnInternet.Text = "Find on &Internet quote site..."
        '
        'mnuFileDivider2
        '
        Me.mnuFileDivider2.Name = "mnuFileDivider2"
        Me.mnuFileDivider2.Size = New System.Drawing.Size(256, 6)
        '
        'mnuFileRandomQuote
        '
        Me.mnuFileRandomQuote.Name = "mnuFileRandomQuote"
        Me.mnuFileRandomQuote.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.R), System.Windows.Forms.Keys)
        Me.mnuFileRandomQuote.Size = New System.Drawing.Size(259, 22)
        Me.mnuFileRandomQuote.Text = "&Random quote"
        '
        'mnuFileByNumber
        '
        Me.mnuFileByNumber.Name = "mnuFileByNumber"
        Me.mnuFileByNumber.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.M), System.Windows.Forms.Keys)
        Me.mnuFileByNumber.Size = New System.Drawing.Size(259, 22)
        Me.mnuFileByNumber.Text = "Nu&mbered quote..."
        '
        'mnuFileFirstNumericQuote
        '
        Me.mnuFileFirstNumericQuote.Name = "mnuFileFirstNumericQuote"
        Me.mnuFileFirstNumericQuote.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.H), System.Windows.Forms.Keys)
        Me.mnuFileFirstNumericQuote.Size = New System.Drawing.Size(259, 22)
        Me.mnuFileFirstNumericQuote.Text = "&First numeric quote"
        '
        'mnuFilePreviousNumericQuote
        '
        Me.mnuFilePreviousNumericQuote.Name = "mnuFilePreviousNumericQuote"
        Me.mnuFilePreviousNumericQuote.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.J), System.Windows.Forms.Keys)
        Me.mnuFilePreviousNumericQuote.Size = New System.Drawing.Size(259, 22)
        Me.mnuFilePreviousNumericQuote.Text = "&Previous numeric quote"
        '
        'mnuFileNextNumericQuote
        '
        Me.mnuFileNextNumericQuote.Name = "mnuFileNextNumericQuote"
        Me.mnuFileNextNumericQuote.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.K), System.Windows.Forms.Keys)
        Me.mnuFileNextNumericQuote.Size = New System.Drawing.Size(259, 22)
        Me.mnuFileNextNumericQuote.Text = "&Next numeric quote"
        '
        'mnuFileLastNumericQuote
        '
        Me.mnuFileLastNumericQuote.Name = "mnuFileLastNumericQuote"
        Me.mnuFileLastNumericQuote.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.L), System.Windows.Forms.Keys)
        Me.mnuFileLastNumericQuote.Size = New System.Drawing.Size(259, 22)
        Me.mnuFileLastNumericQuote.Text = "&Last numeric quote"
        '
        'mnuFileDivider3
        '
        Me.mnuFileDivider3.Name = "mnuFileDivider3"
        Me.mnuFileDivider3.Size = New System.Drawing.Size(256, 6)
        '
        'mnuFileQuit
        '
        Me.mnuFileQuit.Name = "mnuFileQuit"
        Me.mnuFileQuit.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Q), System.Windows.Forms.Keys)
        Me.mnuFileQuit.Size = New System.Drawing.Size(259, 22)
        Me.mnuFileQuit.Text = "E&xit"
        '
        'mnuEdit
        '
        Me.mnuEdit.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuEditUndo, Me.mnuEditBlank1, Me.mnuEditCut, Me.mnuEditCopy, Me.mnuEditPaste, Me.mnuEditDivider2, Me.mnuEditPreferences})
        Me.mnuEdit.Name = "mnuEdit"
        Me.mnuEdit.Size = New System.Drawing.Size(39, 20)
        Me.mnuEdit.Text = "&Edit"
        '
        'mnuEditUndo
        '
        Me.mnuEditUndo.Enabled = False
        Me.mnuEditUndo.Name = "mnuEditUndo"
        Me.mnuEditUndo.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Z), System.Windows.Forms.Keys)
        Me.mnuEditUndo.Size = New System.Drawing.Size(184, 22)
        Me.mnuEditUndo.Text = "&Undo"
        '
        'mnuEditBlank1
        '
        Me.mnuEditBlank1.Name = "mnuEditBlank1"
        Me.mnuEditBlank1.Size = New System.Drawing.Size(181, 6)
        '
        'mnuEditCut
        '
        Me.mnuEditCut.Enabled = False
        Me.mnuEditCut.Name = "mnuEditCut"
        Me.mnuEditCut.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.X), System.Windows.Forms.Keys)
        Me.mnuEditCut.Size = New System.Drawing.Size(184, 22)
        Me.mnuEditCut.Text = "Cu&t"
        '
        'mnuEditCopy
        '
        Me.mnuEditCopy.Name = "mnuEditCopy"
        Me.mnuEditCopy.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.C), System.Windows.Forms.Keys)
        Me.mnuEditCopy.Size = New System.Drawing.Size(184, 22)
        Me.mnuEditCopy.Text = "&Copy"
        '
        'mnuEditPaste
        '
        Me.mnuEditPaste.Name = "mnuEditPaste"
        Me.mnuEditPaste.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.V), System.Windows.Forms.Keys)
        Me.mnuEditPaste.Size = New System.Drawing.Size(184, 22)
        Me.mnuEditPaste.Text = "&Paste..."
        '
        'mnuEditDivider2
        '
        Me.mnuEditDivider2.Name = "mnuEditDivider2"
        Me.mnuEditDivider2.Size = New System.Drawing.Size(181, 6)
        '
        'mnuEditPreferences
        '
        Me.mnuEditPreferences.Enabled = False
        Me.mnuEditPreferences.Name = "mnuEditPreferences"
        Me.mnuEditPreferences.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.mnuEditPreferences.Size = New System.Drawing.Size(184, 22)
        Me.mnuEditPreferences.Text = "P&references..."
        '
        'mnuHelp
        '
        Me.mnuHelp.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuHelpAbout})
        Me.mnuHelp.Name = "mnuHelp"
        Me.mnuHelp.Size = New System.Drawing.Size(44, 20)
        Me.mnuHelp.Text = "&Help"
        '
        'mnuHelpAbout
        '
        Me.mnuHelpAbout.Name = "mnuHelpAbout"
        Me.mnuHelpAbout.ShortcutKeys = System.Windows.Forms.Keys.F1
        Me.mnuHelpAbout.Size = New System.Drawing.Size(176, 22)
        Me.mnuHelpAbout.Text = "&About Quotes..."
        '
        'mnuFormRightClick
        '
        Me.mnuFormRightClick.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFormRightClickCopy, Me.mnuFormRightClickPaste, Me.mnuFormRighClickDiv1, Me.mnuFormRightClickFind, Me.mnuFormRightClickFindNext, Me.mnuFormRightClickFindSelection, Me.mnuFormRightClickFindAttribution, Me.mnuFormRightClickFindNextAttribution})
        Me.mnuFormRightClick.Name = "mnuFormRightClick"
        Me.mnuFormRightClick.Size = New System.Drawing.Size(101, 20)
        Me.mnuFormRightClick.Text = "FormRightClick"
        Me.mnuFormRightClick.Visible = False
        '
        'mnuFormRightClickCopy
        '
        Me.mnuFormRightClickCopy.Name = "mnuFormRightClickCopy"
        Me.mnuFormRightClickCopy.Size = New System.Drawing.Size(181, 22)
        Me.mnuFormRightClickCopy.Text = "Copy"
        '
        'mnuFormRightClickPaste
        '
        Me.mnuFormRightClickPaste.Name = "mnuFormRightClickPaste"
        Me.mnuFormRightClickPaste.Size = New System.Drawing.Size(181, 22)
        Me.mnuFormRightClickPaste.Text = "Paste..."
        '
        'mnuFormRighClickDiv1
        '
        Me.mnuFormRighClickDiv1.Name = "mnuFormRighClickDiv1"
        Me.mnuFormRighClickDiv1.Size = New System.Drawing.Size(178, 6)
        '
        'mnuFormRightClickFind
        '
        Me.mnuFormRightClickFind.Enabled = False
        Me.mnuFormRightClickFind.Name = "mnuFormRightClickFind"
        Me.mnuFormRightClickFind.Size = New System.Drawing.Size(181, 22)
        Me.mnuFormRightClickFind.Text = "Find..."
        '
        'mnuFormRightClickFindNext
        '
        Me.mnuFormRightClickFindNext.Enabled = False
        Me.mnuFormRightClickFindNext.Name = "mnuFormRightClickFindNext"
        Me.mnuFormRightClickFindNext.Size = New System.Drawing.Size(181, 22)
        Me.mnuFormRightClickFindNext.Text = "Find Next"
        '
        'mnuFormRightClickFindSelection
        '
        Me.mnuFormRightClickFindSelection.Enabled = False
        Me.mnuFormRightClickFindSelection.Name = "mnuFormRightClickFindSelection"
        Me.mnuFormRightClickFindSelection.Size = New System.Drawing.Size(181, 22)
        Me.mnuFormRightClickFindSelection.Text = "Find selection"
        '
        'mnuFormRightClickFindAttribution
        '
        Me.mnuFormRightClickFindAttribution.Enabled = False
        Me.mnuFormRightClickFindAttribution.Name = "mnuFormRightClickFindAttribution"
        Me.mnuFormRightClickFindAttribution.Size = New System.Drawing.Size(181, 22)
        Me.mnuFormRightClickFindAttribution.Text = "Find attribution"
        '
        'mnuFormRightClickFindNextAttribution
        '
        Me.mnuFormRightClickFindNextAttribution.Enabled = False
        Me.mnuFormRightClickFindNextAttribution.Name = "mnuFormRightClickFindNextAttribution"
        Me.mnuFormRightClickFindNextAttribution.Size = New System.Drawing.Size(181, 22)
        Me.mnuFormRightClickFindNextAttribution.Text = "Find next attribution"
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 30000
        '
        'lblAttribution
        '
        Me.lblAttribution.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblAttribution.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.lblAttribution.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAttribution.Font = New System.Drawing.Font("Tempus Sans ITC", 18.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAttribution.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAttribution.Location = New System.Drawing.Point(5, 347)
        Me.lblAttribution.Name = "lblAttribution"
        Me.lblAttribution.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAttribution.Size = New System.Drawing.Size(651, 33)
        Me.lblAttribution.TabIndex = 1
        Me.lblAttribution.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.lblAttribution.UseMnemonic = False
        '
        'lblQuote
        '
        Me.lblQuote.AllowDrop = True
        Me.lblQuote.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblQuote.AutoEllipsis = True
        Me.lblQuote.BackColor = System.Drawing.SystemColors.Window
        Me.lblQuote.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblQuote.Font = New System.Drawing.Font("Tempus Sans ITC", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQuote.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblQuote.Location = New System.Drawing.Point(0, 28)
        Me.lblQuote.Name = "lblQuote"
        Me.lblQuote.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblQuote.Size = New System.Drawing.Size(668, 319)
        Me.lblQuote.TabIndex = 0
        Me.lblQuote.Text = "One moment..."
        Me.lblQuote.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblQuote.UseCompatibleTextRendering = True
        '
        'FrmQuote
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Window
        Me.ClientSize = New System.Drawing.Size(668, 389)
        Me.Controls.Add(Me.MainMenu1)
        Me.Controls.Add(Me.lblAttribution)
        Me.Controls.Add(Me.lblQuote)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Location = New System.Drawing.Point(11, 30)
        Me.Name = "FrmQuote"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Quote"
        Me.MainMenu1.ResumeLayout(False)
        Me.MainMenu1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
#End Region
End Class