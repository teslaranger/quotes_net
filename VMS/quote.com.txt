$! Set NoVerify
$!---------------------------------------------------------------------------
$! QUOTE.COM
$!
$! Written by Bill Davis
$!
$! Displays a quote from the file QUOTES.TXT. Quotes must be on a single line,
$! start with a " as the first character, and should be followed by an
$! attribution line if possible, e.g.:
$!
$! "This is the first quote.  Isn't it insightful?"
$!   - John Quotesmith
$!
$! "This is the second quote.  Isn't it funny?"
$!   - Luke Quotewalker
$!
$! ....
$!
$! Modification History:
$! 2005/12/21  WBD   Created first version
$! 2005/12/22  WBD   Fixed a couple of bugs, added more documentation.
$!                   1. Could get into infinite loop because retry_count was
$!                      being zeroed in wrong place.  Moved to top.
$!                   2. All the f$cvtime() calls need f$integer() in front,
$!                      as f$cvtime returns string not integer.  Thus if
$!                      hh = 01 and ss = 02 then hh+ss = 0102 not 3.
$! 2005/12/29  WBD   Modified code that uses "OFYEAR" feature of f$cvtime
$!                    as that doesn't work on VMS 7.1 but does on 7.3.
$!                    Approximates day/hour/minute/second of year instead.
$!                   Added version # display ( "1." + .com file version)
$! 2006/03/08  WBD   Added random number generation subroutine RAND from
$!                    dcl.openvms.org
$!                   Added fix for attribution length being > quote length.
$!                    when drawing dashed lines above/below quote.
$!                   Added COUNT_QUOTES subroutine to set max # of quotes and
$!                    set the ceiling for the random number generator. The
$!                    downside is it slows the program down on large files.
$!---------------------------------------------------------------------------
$!
$! Extract date/time info to variables for randomizing selection
$!
$STARTUP:
$ qver = f$parse(f$environment("procedure"),,,"VERSION")
$ qver = f$extract(1,5,qver)
$ GOSUB COUNT_QUOTES
$ qn = 0
$ retry_count = 0
$!
$GET_TIMES:
$ tt = f$time()
$ hh = f$integer(f$cvtime(tt,,"HOUR"))
$ mm = f$integer(f$cvtime(tt,,"MINUTE"))
$ ss = f$integer(f$cvtime(tt,,"SECOND"))
$ cc = f$integer(f$cvtime(tt,,"HUNDREDTH"))
$ mn = f$integer(f$cvtime(tt,,"MONTH"))
$ dy = f$integer(f$cvtime(tt,,"DAY"))
$ yr = f$integer(f$cvtime(tt,,"YEAR"))
$!
$! *OFYEAR do not work on VMS 7.1 but do in 7.3
$ If (f$extract(0,4,f$getsyi("version")) .eqs. "V7.3")
$ Then
$   doy = f$integer(f$cvtime(tt,,"DAYOFYEAR"))
$   hoy = f$integer(f$cvtime(tt,,"HOUROFYEAR"))
$   moy = f$integer(f$cvtime(tt,,"MINUTEOFYEAR"))
$   soy = f$integer(f$cvtime(tt,,"SECONDOFYEAR"))
$ Else
$   ! approximate the *OFYEAR values
$   doy = (mn-1)*30+dy
$   hoy = doy*24+hh
$   moy = hoy*60+mm
$   soy = moy*60+ss
$ EndIf
$!
$CHECK_PARAMETERS:
$ If (P1 .eqs. "RANDOM") .or. (P1 .eqs. "")
$ Then
$!   qn = (hh + mm + ss)
$    GOSUB RAND
$    qn = RANDOM
$ EndIf
$!
$ If (P1 .eqs. "DAILY")
$ Then
$   qn = doy
$ EndIf
$!
$ If (P1 .eqs. "NUMBER")
$ Then
$   qn = F$Integer(p2)
$ EndIf
$!
$ If (F$Integer(P1) .ne. 0)
$ Then
$   qn = F$Integer(P1)
$ EndIf
$!
$ If qn .eq. 0
$ Then
$    GOSUB RAND
$    qn = RANDOM
$ EndIf
$!
$! Now grab the nth quote in quotes.txt, using qn (quote number) as the index
$!
$GET_QUOTE:
$  qc  = 0
$  ql1 = ""
$  ql2 = "  - Unknown"
$!
$  Open/Read/Share/Error=OPEN_ERRHANDLER qf quotes.txt
$!
$! Read lines until we find the qnTH line starting with a (") mark.
$!
$READ_LOOP:
$  Read/NoLock/End_Of_File=EOF_ERRHANDLER/Error=READ_ERRHANDLER qf ql1
$  If f$extract(0,1,ql1) .eqs. """" then qc = qc + 1
$  If qc .ne. qn Then GoTo read_loop
$!
$! After selecting a quote, get the next line, which is the quote's
$! attribution line.  If no next line, output the quote without
$! an attribute line.
$!
$  Read/NoLock/End_Of_File=OUTPUT_QUOTE/Error=OUTPUT_QUOTE qf ql2
$!
$OUTPUT_QUOTE:
$  qlen = F$Length(ql1)
$  If F$Length(ql2) .gt. qlen Then qlen=F$Length(ql2)
$  width=F$GETDVI("TT","DEVBUFSIZ")  ! get terminal width
$  if (qlen .ge. width) then qlen = width
$  Write sys$output ""
$  Write Sys$output "Quote  v1.",qver,"  #",qc,"/",CEIL
$  Write Sys$output f$fao("!#*_", qlen)
$  Write Sys$output ql1
$  Write sys$output ql2
$  Write Sys$output f$fao("!#*_", qlen)
$  Write sys$output ""
$!
$EXXIT:
$  Close qf
$  Exit
$!
$!-----------------------------------------------------------------------------
$! Error Handlers
$!
$OPEN_ERRHANDLER:
$  Write Sys$Output "QUOTE-F-FILNOTFOU, Unable to open Quotes.txt file!  ", $status, $severity
$  Goto Exxit
$!
$READ_ERRHANDLER:
$  Write Sys$Output "QUOTE-F-READERR, Unable to read Quotes.txt file!  ",$status, $severity
$  Goto Exxit
$!
$EOF_ERRHANDLER:
$  Close qf
$  retry_count=retry_count+1
$  If retry_count .ge. 2 Then GoTo READ_ERRHANDLER
$!
$  qn = (hh+mm+ss)
$  GoTo GET_QUOTE
$!
$!-----------------------------------------------------------------------------
$COUNT_QUOTES:
$!
$  Open/Read/Share/Error=OPEN_ERRHANDLER qf quotes.txt
$!
$! Read lines until we run out of lines, counting quotes.
$!
$  qc = 0
$!
$COUNT_LOOP:
$  Read/NoLock/End_Of_File=COUNT_DONE/Error=COUNT_DONE qf ql1
$  If f$extract(0,1,ql1) .eqs. """" then qc = qc + 1
$  GoTo count_loop
$!
$COUNT_DONE:
$  Close qf
$  CEIL = qc + 1
$  qc = 0
$  Return
$!
$!-----------------------------------------------------------------------------
$!        __CEIL - 1.  If CEIL is provided, it is used as a new __CEIL.
$!        Based on the original work provided in the OpenVMS FAQ.
$ RAND:
$       IF F$TYPE(__SEED) .EQS. ""
$        THEN
$! seed the random number generator, ...
$         __SEED == (10*(f$cvtime(,,"hundredth") + f$cvtime(,,"second") + -
f$cvtime(,,"minute") + f$cvtime(,,"hour") + -
f$cvtime(,,"day")  -
.AND. %X7FFFFFFF) .OR. 1)
$       ENDIF
$
$       If CEIL .nes. "" then __CEIL = F$Integer(CEIL)
$       IF F$TYPE(__CEIL) .EQS. "" THEN __CEIL = %X3FFFFFFF
$! the generator tends to do better with a large, odd seed, ...
$       __SEED == (((__SEED * 69069) .and. %x7ffffffe) + 1)
$       RANDOM == (__SEED .and. %x3fffffff)/(%X40000000/__CEIL)
$       RETURN
$!-----------------------------------------------------------------------------
$! End of Quote.Com
$!-----------------------------------------------------------------------------
$! Ideas:
$!   Search for quotes by a particular person:  QUOTE AUTHOR Bierce
$!   Search for quotes containing particular string:  QUOTE SEARCH destiny
$!   Support quotes bigger than string limit (presently causes an error and is truncated)
$!   Specify a quote file to use:   QUOTE /FILE=filespec
$!   Support parameters with and without "/"   e.g. QUOTE RANDOM   QUOTE /RANDOM
$!      QUOTE AUTHOR Bierce    QUOTE AUTHOR=Bierce     QUOTE /AUTHOR=Bierce
$!   Display date info (and "of year" into above quote
$!   Get name used to invoke (QUOTE, FORTUNE,COOKIE) and display that instead of "Quote"
$!    above the quote.
$!   Fully "box" in quote:
$!      _________________________________   +---------------+
$!      | "This is an excellent quote." |   |               |
$!      |   - John Smith                |   |               |
$!      |_______________________________|   +---------------+
$!   Support both ASCII line drawing chars and VT-100 graphical line drawing chars
$!   Display quote in VT-100 bold, attribute plain.
$!   Use a random charcter instead of ______ to surround quote:
$!      ___  ---  ~~~~~ ######  $$$$$$$ ^^^^^ \\\\\\ ////// ...... >>>>>> <<<<<< ***** etc
$!
$!
