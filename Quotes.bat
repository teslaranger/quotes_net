@Echo off
:: QUOTE.BAT
::
:: Display a random quote from a text file formatted as follows:
::
::--------------------------------------------------------
:: "This is a quote.......it must be on a single line"
::   - Who said this
::
::
:: "This is a second quote....it's 5 lines down from the previous quote
::  - Who said it
::----EOF-------------------------------------------------

Set FS_QUOTES="quotes.txt"
::TODO: get current dir of this batch file and add to FS_QUOTES

::Set QN=%RANDOM%
Set QN=3
Set /A QA=%QN%+1
Set QL=0

Echo Quote #%QN%
Echo ------------------------------------------------------------------------------------------------------------

For /F "tokens=* delims=^" %%a in (quotes.txt) do (Call :DisplayQuote %%a %%b)

Echo ------------------------------------------------------------------------------------------------------------

:Exxit
Pause
(Goto:EOF)
Exit

:DisplayQuote
@SET /A QL=%QL%+1
if %QL% GTR %QA% (exit /b 0)
if %QL% GTR %QA% (goto:eof)
@Set QL
@Set QA
@Set QN
@echo "%1="%1
@echo "%2="%2
@echo "%3="%3
set qs=%1
if not .%2 == . set qs = %qs% %2
if not .%3 == . set qs = %qs% %3
set qs
if %QL% EQU %QN% @echo %1 %2 
if %QL% EQU %QA% @echo %1 %2
pause
(goto:eof)
